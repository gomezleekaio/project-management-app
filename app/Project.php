<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function employee()
    {
    	return $this->belongsTo('App\Employee', 'owner_id');
    }

    public function department()
    {
    	return $this->belongsTo('App\Department');
    }

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
