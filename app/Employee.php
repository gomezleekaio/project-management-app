<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function department()
    {
    	return $this->belongsTo('App\Department');
    }

    public function role()
    {
    	return $this->belongsTo('App\Role');
    }

    public function projects()
    {
    	return $this->hasMany('App\Project', 'owner_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task', 'member_id');
    }
}
