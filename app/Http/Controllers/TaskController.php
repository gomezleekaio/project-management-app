<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;
use App\Project;
use App\Employee;

class TaskController extends Controller
{
    public function index()
    {
    	$tasks = Task::all();
    	return view('tasks', compact('tasks'));
    }

    public function create($id)
    {
    	$project = Project::find($id);
    	$employees = Employee::where('role_id', 3)
    					->orWhere('role_id', 4)
    					->orWhere('role_id', 5)
    					->get();
    	return view('add-task', compact('project', 'employees'));
    }

    public function store($id, Request $request)
    {

        $allTasks = Task::where('project_id', $id)->get();

        if ($allTasks->isEmpty()) {
            $project = Project::find($id);
            $project->status_id = 2;
            $project->save();
        }

    	$task = new Task;
    	$task->name = $request->get('name');
    	$task->description = $request->get('description');
    	$task->deadline = $request->deadline;
    	$task->member_id = $request->get('member_id');
    	$task->project_id = $id;
    	$task->status_id = 1;
    	$task->save();
    	return redirect("/projects/$id");
    }

    public function edit($id)
    {
        $task = Task::find($id);
        $project = Project::find($task->project_id);
        $employees = Employee::all();
        return view('edit-task', compact('task', 'project', 'employees'));
    }

    public function update($id, Request $request)
    {
        $task = Task::find($id);
        $task->name = $request->get('name');
        $task->description = $request->get('description');
        $task->deadline = $request->get('deadline');
        $task->member_id = $request->get('member_id');
        $task->save();
        $project_id = $task->project_id;
        return redirect("/projects/$project_id");
    }

    public function updateStatus($id, Request $request)
    {
        $task = Task::find($id);
        $task->status_id = $request->get('status_id');
        $task->save();
        $project_id = $task->project_id;
        return redirect("/projects/$project_id");
    }

    public function destroy(Request $request)
    {
    	Task::destroy($request->get('id'));
    	$id = $request->project_id;
    	return redirect("/projects/$id");
    }
}
