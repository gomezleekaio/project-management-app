<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Department;
use App\Employee;
use App\Task;
use App\Status;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        if (isset($request) && $request->get('status_id') > 0) {

            $statuses = Status::all();
            $departments = Department::all();
            $projects = Project::where('status_id', $request->get('status_id'))->get();
            $statusID = $request->get('status_id');
            return view('projects', compact('projects', 'statuses', 'departments', 'statusID'));
        } else {
            $projects = Project::all();
            $statuses = Status::all();
            $departments = Department::all();
            return view('projects', compact('projects', 'statuses', 'departments'));
        }
    }

    public function create()
    {
    	$departments = Department::all();
    	$owners = Employee::all();
    	return view('add-project', compact('departments', 'owners'));
    }
    

    public function store(Request $request)
    {
    	$project = new Project;
    	$project->name = $request->get('name');
    	$project->description = $request->get('description');
    	$project->deadline = $request->get('deadline');
    	$project->department_id = $request->get('department_id');
    	$project->owner_id = $request->get('owner_id');
    	$project->status_id = 1;
    	$project->save();
    	return redirect('/projects');
    }

    public function edit($id)
    {
        $project = Project::find($id);
        $departments = Department::all();
        $owners = Employee::all();
        return view('edit-project', compact('project', 'departments', 'owners'));
    }

    public function update($id, Request $request)
    {
        $project = Project::find($id);
        $project->name = $request->get('name');
        $project->description = $request->get('description');
        $project->deadline = $request->get('deadline');
        $project->department_id = $request->get('department_id');
        $project->owner_id = $request->get('owner_id');
        $project->save();
        return redirect("/projects/$id");
    }

    public function updateStatus($id, Request $request)
    {
        $project = Project::find($id);
        if($project->tasks->count() > 0) {
            $project->status_id = $request->get('status_id');
            $project->save();
        }
        return redirect("/projects/$id");
    }

    public function destroy($id)
    {
    	Project::destroy($id);
    	return redirect('/projects');
    }

    public function view_project($id)
    {
        $project = Project::find($id);
        $tasks = $project->tasks;
        $statuses = Status::all();
        $statusArray = $tasks->pluck('status_id');
        $finished = 0;
        foreach ($statusArray as $status) {
            if ($status != 4) {
                $finished++;
            }
        }

        return view('project', compact('project', 'tasks', 'statuses', 'finished'));
    }

    // public function filter_projects_by_status(Request $request)
    // {
    //     $projects = Project::where('status_id', $request->get('status_id'))->get();
    //     $statuses = Status::all();
    //     $departments = Department::all();
    //     $statusID = $request->get('status_id');
    //     return view('/projects', compact('projects', 'statuses', 'departments', 'statusID'));
    // }

    public function filter_projects_by_department(Request $request)
    {
        $projects = Project::where('department_id', $request->get('department_id'))->get();
        $statuses = Status::all();
        $departments = Department::all();
        return view('projects', compact('projects', 'statuses', 'departments'));
    }
}
