<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Role;
use App\Department;

class EmployeeController extends Controller
{
    public function index()
    {
    	$employees = Employee::all();
    	return view('employees', compact('employees'));
    }

    public function create()
    {
    	$departments = Department::all();
    	$roles = Role::all();
    	return view('add-employee', compact('departments', 'roles'));
    }

    public function store(Request $request)
    {

    	$rules = [
    		'firstname' => 'required',
    		'lastname' => 'required',
    		'email' => 'required',
    		'department_id' => 'required',
    		'role_id' => 'required'
    	];

    	$this->validate($request, $rules);

    	$employee = new Employee;
    	$employee->firstname = $request->get('firstname');
    	$employee->lastname = $request->get('lastname');
    	$employee->email = $request->get('email');
    	$employee->department_id = $request->get('department_id');
    	$employee->role_id = $request->get('role_id');
    	$employee->save();
    	return redirect('/employees');
    }

    public function destroy(Request $request)
    {
        Employee::destroy($request->get('id'));
        return redirect('/employees');
    }
}
