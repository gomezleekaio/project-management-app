<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function employee()
    {
    	return $this->belongsTo('App\Employee', 'member_id');
    }

    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function project()
    {
    	return $this->belongsTo('App\Project');
    }
}
