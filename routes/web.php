<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProjectController@index');

Route::get('/employees', 'EmployeeController@index');
Route::get('/add-employee', 'EmployeeController@create');
Route::post('/add-employee', 'EmployeeController@store');
Route::delete('/delete-employee', 'EmployeeController@destroy');

Route::get('/projects', 'ProjectController@index');
Route::post('/projects', 'ProjectController@index');
Route::get('/add-project', 'ProjectController@create');
Route::get('/edit-project/{id}', 'ProjectController@edit');
Route::patch('/edit-project/{id}', 'ProjectController@update');
Route::patch('/update-project-status/{id}', 'ProjectController@updateStatus');
// Route::get('/filter-projects-by-status', 'ProjectController@filter_projects_by_status');
Route::get('/filter-projects-by-departments', 'ProjectController@filter_projects_by_department');


Route::get('/projects/{id}', 'ProjectController@view_project');

Route::post('/add-project', 'ProjectController@store');

Route::delete('delete-project/{id}', 'ProjectController@destroy');

Route::get('/tasks', 'TaskController@index');
Route::get('/edit-task/{id}', 'TaskController@edit');
Route::patch('/edit-task/{id}', 'TaskController@update');

Route::get('/add-task/{id}', 'TaskController@create');
Route::post('/add-task/{id}', 'TaskController@store');
Route::delete('/delete-task', 'TaskController@destroy');
Route::patch('/update-status/{id}', 'TaskController@updateStatus');