

INSERT INTO statuses(name) VALUES ('pending');
INSERT INTO statuses(name) VALUES ('on-going');
INSERT INTO statuses(name) VALUES ('on-hold');
INSERT INTO statuses(name) VALUES ('finished');


INSERT INTO projects(name, description, deadline, owner_id, status_id, department_id) VALUES ('Halloween Party', 'Not your ordinary party.', '10/31/2019', 1, 2, 2);


INSERT INTO departments(name) VALUES ('Administrative Department');
INSERT INTO departments(name) VALUES ('IT Department');
INSERT INTO departments(name) VALUES ('Party Planning Department');
INSERT INTO departments(name) VALUES ('Biohazard Department');
INSERT INTO departments(name) VALUES ('Language Department');


INSERT INTO roles(name) VALUES ('Superadmin');
INSERT INTO roles(name) VALUES ('Admin');
INSERT INTO roles(name) VALUES ('Project Manager');
INSERT INTO roles(name) VALUES ('Department Manager');
INSERT INTO roles(name) VALUES ('Project Member');

INSERT INTO employees(firstname, lastname, email, role_id, department_id) VALUES('Brandon', 'Brandon', 'brandon@brandon.com', 3, 2);
INSERT INTO employees(firstname, lastname, email, role_id, department_id) VALUES('Mimi', 'Yuh', 'brandon@brandon.com', 5, 4);