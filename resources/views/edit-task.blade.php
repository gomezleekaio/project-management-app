
@extends('templates.template')

@section('title', 'Edit Task')

@section('content')
	<h1 class="text-center py-5">Edit Task</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="/edit-task/{{$task->id}}" method="POST">
			@csrf
			{{method_field('PATCH')}}
			<div class="form-group">
				<label for="name">Task Name</label>
				<input type="text" name="name" class="form-control" value="{{$task->name}}">
			</div>
			<div class="form-group">
				<label for="description">Task Description</label>
				<textarea name="description" class="form-control">{{$task->description}}</textarea>
			</div>
			<div class="form-group">
				<label for="deadline">Task Deadline</label>
				<input type="date" name="deadline" class="form-control" value="{{$task->deadline}}">
			</div>
			<div class="form-group">
				<label for="member_id">Assign To:</label>
				<select name="member_id" class="form-control">
					@foreach($employees as $employee)
					<option value="{{$employee->id}}"
						{{$employee->id == $task->member_id ? "selected": ""}}
						>{{$employee->firstname ." ". $employee->lastname}}</option>
						}
					@endforeach
				</select>
			</div>
			<button type="submit" class="btn btn-success">Update Task</button>
		</form>
	</div>
@endsection