@extends('templates.template')

@section('title', 'Employees')

@section('content')
	<h1 class="text-center py-5">Employees</h1>
	<div class="col-lg-10 offset-lg-1">
		<table class="table table-striped border">
			<thead>
				<tr>
					<th>Employee ID</th>
					<th>Employee Name</th>
					<th>Employee Email</th>
					<th>Employee Department</th>
					<th>Employee Role</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($employees as $employee)
				<tr>
					<td>{{$employee->id}}</td>
					<td>{{$employee->firstname . " " . $employee->lastname}}</td>
					<td>{{$employee->email}}</td>
					<td>{{$employee->department->name}}</td>
					<td>{{$employee->role->name}}</td>
					<td>
						@if($employee->projects->count() < 1 && $employee->tasks->count() < 1)
							<form action="/delete-employee" method="POST">
								@csrf
								{{method_field('DELETE')}}
								<input type="hidden" name="id" value="{{$employee->id}}">
								<button type="submit" class="btn btn-danger">Delete Employee</button>
							</form>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection