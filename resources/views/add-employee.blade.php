@extends('templates.template')

@section('title', 'Add Employee')

@section('content')
	<h1 class="text-center py-5">Projects</h1>
	<div class="col-lg-6 offset-lg-3">
		<form action="/add-employee" method="POST">
			@csrf
			<div class="form-group">
				<label for="firstname">First Name:</label>
				<input type="text" name="firstname" class="form-control">
			</div>
			<div class="form-group">
				<label for="lastname">Last Name:</label>
				<input type="text" name="lastname" class="form-control">
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="text" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label for="department_id">Department</label>
				<select name="department_id" class="form-control">
					@foreach($departments as $department)
						<option value="{{$department->id}}">{{$department->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="role_id">Role:</label>
				<select name="role_id" class="form-control">
					@foreach($roles as $role)
						@if($role->id != 1)
							<option value="{{$role->id}}">{{$role->name}}</option>
						@endif
					@endforeach
				</select>
			</div>
			<button type="submit" class="btn btn-primary">Add Employee</button>
		</form>
	</div>
	
@endsection