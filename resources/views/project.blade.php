
@extends('templates.template')

@section('title', 'Project Details')

@section('content')
	<h1 class="text-center py-5">Success</h1>
	<div class="col-lg-6 offset-lg-3">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">
					Project Name: {{$project->name}}
				</h5>
				<p class="card-text">
					Description: {{ $project->description }}
				</p>
				<p class="card-text">
					Deadline: {{ $project->deadline }}
				</p>
				<p class="card-text">
					Department: {{$project->department->name}}
				</p>
				{{-- <p class="card-text">
					Status: 
					<form action="/update-project-status/{{$project->id}}" method="POST">
						@csrf
						{{method_field('PATCH')}}
						<select name="status_id" class="form-control">
							@foreach($statuses as $status)
							<option 
								value="{{$status->id}}"
								{{$status->id == $project->status_id ? "selected": ""}}
								>
								{{$status->name}}
							</option>
							@endforeach
						</select>
						<button type="submit" class="btn btn-warning">Update Status</button>
					</form>
				</p> --}}
				<p class="card-text">
					Project Owner: {{ $project->employee->firstname ." ". $project->employee->lastname }}
				</p>
				<div class="card-footer">
					<a href="/add-task/{{$project->id}}" class="btn btn-info">Add Task</a>
					<a href="/edit-project/{{$project->id}}" class="btn btn-success">Edit Project</a>
				</div>
				<div class="card-footer">
					<form action="/update-project-status/{{$project->id}}" method="POST">
						@csrf
						{{method_field('PATCH')}}
						<select name="status_id" class="form-control"
							@php
								$unfinished = $project->tasks->where('status_id', '!=', 4);
							@endphp
							@if($project->tasks->count() > 0)
							@else
								disabled
							@endif
						>
							@foreach($statuses as $status)
							<option 
								value="{{$status->id}}"
								{{$status->id == $project->status_id ? "selected": ""}}
								{{-- @if(isset($finished)) --}}
									@if(count($unfinished) > 0 && $status->id == 4)
										disabled 
									@endif
								{{-- @endif --}}
								>
								{{$status->name}}
							</option>
							@endforeach
						</select>
						<button type="submit" class="btn btn-success">Update Status</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-10 offset-lg-1">
		<table class="table table-striped border">
			<thead>
				<tr>
					<th>Task ID</th>
					<th>Task Name</th>
					<th>Task Deadline</th>
					<th>Task Assignee</th>
					<th>Task Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($tasks as $task)
					<tr>
						<td>002-04{{$project->id}}-63{{$task->id}}</td>
						<td>{{$task->name}}</td>
						<td>{{$task->deadline}}</td>
						<td>{{$task->employee->firstname ." ". $task->employee->lastname}}</td>
						<td>
							<form action="/update-status/{{$task->id}}" method="POST">
								@csrf
								{{method_field('PATCH')}}
								<select name="status_id" class="form-control">
									@foreach($statuses as $status)
									<option 
										value="{{$status->id}}"
										{{$status->id == $task->status_id ? "selected": ""}}
										>
										{{$status->name}}
									</option>
									@endforeach
								</select>
								<button type="submit" class="btn btn-warning">Update Status</button>
							</form>
						</td>
						<td>
							<a href="/edit-task/{{$task->id}}" class="btn btn-primary">Edit Task</a>
							<form action="/delete-task" method="POST">
								@csrf
								{{method_field('DELETE')}}
								<input type="hidden" name="id" value="{{$task->id}}">
								<input type="hidden" name="project_id" value="{{$project->id}}">
								<button type="submit" class="btn btn-danger">Delete Task</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection