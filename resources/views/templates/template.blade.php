<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>

	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	  <a class="navbar-brand" href="/">B46</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor01">
	    <ul class="navbar-nav mr-auto">
	    	<li class="nav-item active">
		      <a class="nav-link" href="/projects">Projects</a>
		    </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="/add-project">Add Project</a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="/add-employee">Add Employee</a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="/employees">Show Employees</a>
	      </li>
	    </ul>
	  </div>
	</nav>

	@yield('content')

	<footer class="footer bg-dark">
		<div class="container">
			<p class="text-center text-white">
				Made with Love By: Brandon :)
			</p>
		</div>
	</footer>

</body>
</html>