
@extends('templates.template')

@section('title', 'Add Task')

@section('content')
	<h1 class="text-center py-5">Add Task for Project: {{$project->name}} </h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="/add-task/{{$project->id}}" method="POST">
			@csrf
			<div class="form-group">
				<label for="name">Task Name</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Task Description</label>
				<textarea name="description" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label for="deadline">Task Deadline</label>
				<input type="date" name="deadline" class="form-control">
			</div>
			<div class="form-group">
				<label for="member_id">Assign To:</label>
				<select name="member_id" class="form-control">
					@foreach($employees as $employee)
					<option value="{{$employee->id}}">{{$employee->firstname ." ". $employee->lastname}}</option>
					@endforeach
				</select>
			</div>
			<button type="submit" class="btn btn-success">Add Task</button>
		</form>
	</div>
@endsection