@extends('templates.template')

@section('title', 'Add Project')

@section('content')
	<h1 class="text-center py-5">Add Project</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="/add-project" method="POST">
			@csrf
			<div class="form-group">
				<label for="name">Project Name</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Project Description</label>
				<textarea name="description" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label for="deadline">Project Deadline</label>
				<input type="date" name="deadline" class="form-control">
			</div>
			<div class="form-group">
				<label for="department_id">Department</label>
				<select 
					name="department_id" 
					class="form-control">
					@foreach($departments as $department)
					<option value="{{$department->id}}">{{$department->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="owner_id">Project Owner</label>
				<select 
					name="owner_id" 
					class="form-control">
					@foreach($owners as $owner)
					<option value="{{$owner->id}}">
						{{$owner->firstname ." ". $owner->lastname ." of ".$owner->department->name}}
					</option>
					@endforeach
				</select>
			</div>
			<button class="btn btn-success" type="submit">Add Project</button>
		</form>
	</div>
@endsection