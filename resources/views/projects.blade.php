@extends('templates.template')

@section('title', 'Projects')

@section('content')
	<h1 class="text-center py-5">Projects</h1>
	<div class="row">
		<div class="col-lg-2 offsset-lg-1">
			<form action="/projects" method="POST">
				@csrf
				<select name="status_id" class="form-control">
					<option value="0">All</option>
					@foreach($statuses as $status)
					<option 
						value="{{$status->id}}"
						@if(isset($statusID))
							@if($status->id == $statusID)
								selected
							@endif
						@endif
						>
						{{$status->name}}
					</option>
					@endforeach
				</select>
				<button type="submit" class="btn btn-info">Filter by Status</button>
			</form>
		</div>
		<div class="col-lg-2">
			<form action="/filter-projects-by-departments" method="POST">
				@csrf
				{{method_field('GET')}}
				<select name="department_id" class="form-control">
					@foreach($departments as $department)
					<option 
						value="{{$department->id}}"
						@foreach($projects as $project)
							@if($department->id == $project->department_id)
								{{"selected"}}
							@else
								{{""}}
							@endif
						@endforeach
						>
						{{$department->name}}
					</option>
					@endforeach
				</select>
				<button type="submit" class="btn btn-info">Filter by Department</button>
			</form>
		</div>
	</div>
	
	<div class="col-lg-10 offset-lg-1">
		<table class="table table-striped border">
			<thead>
				<tr>
					<th>Project ID</th>
					<th>Project Name</th>
					<th>Department</th>
					<th>Deadline</th>
					<th>Owner</th>
					<th>Members</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($projects as $project)
				<tr>
					<td>521887-00{{$project->id}}</td>
					<td>{{$project->name}}</td>
					<td>{{$project->department->name}}</td>
					<td>{{$project->deadline}}</td>
					<td>{{$project->employee->firstname . " " . $project->employee->lastname}}</td>
					<td>
						<?php $tasks = App\Task::where('project_id', $project->id)->get() ?>
						@foreach($tasks as $task)
							@if($task->project_id == $project->id)
								{{$task->employee->firstname ." ". $task->employee->lastname . ", "}}
							@endif
						@endforeach
					</td>
						
					<td>{{$project->status->name}}</td>
					<td>
						<a href="/projects/{{$project->id}}" class="btn btn-info">View Project</a>
						<form action="/delete-project/{{$project->id}}" method="POST">
							@csrf
							{{ method_field('DELETE')}}
							<button type="submit" class="btn btn-danger">Delete</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
@endsection