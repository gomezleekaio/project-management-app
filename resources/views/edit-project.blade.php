@extends('templates.template')

@section('title', 'Edit Project')

@section('content')
	<h1 class="text-center py-5">Edit Project</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="/edit-project/{{$project->id}}" method="POST">
			@csrf
			{{method_field('PATCH')}}
			<div class="form-group">
				<label for="name">Project Name</label>
				<input type="text" name="name" class="form-control" value="{{$project->name}}">
			</div>
			<div class="form-group">
				<label for="description">Project Description</label>
				<textarea name="description" class="form-control">{{$project->description}}</textarea>
			</div>
			<div class="form-group">
				<label for="deadline">Project Deadline</label>
				<input type="date" name="deadline" class="form-control" value="{{$project->deadline}}">
			</div>
			<div class="form-group">
				<label for="department_id">Department</label>
				<select 
					name="department_id" 
					class="form-control">
					@foreach($departments as $department)
					<option value="{{$department->id}}">{{$department->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="owner_id">Project Owner</label>
				<select 
					name="owner_id" 
					class="form-control">
					@foreach($owners as $owner)
					<option value="{{$owner->id}}">
						{{$owner->firstname ." ". $owner->lastname ." of ".$owner->department->name}}
					</option>
					@endforeach
				</select>
			</div>
			<button class="btn btn-success" type="submit">Update Project</button>
		</form>
	</div>
@endsection